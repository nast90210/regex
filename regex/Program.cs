﻿using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

namespace regex
{
    class Program
    {
        private static readonly string quota = "\"";
        private static readonly string Reg = @"<img(.+)\s*src=\s*\" + quota + @"(.[^\s]+)\" + quota + @"\s?";
        private static readonly string ImageReg = @"\s*\" + quota + @"(.[^\s]+)\.photo\s?4\*\" + quota;
        private static readonly string _url = "https://www.thephoblographer.com/2017/07/06/the-7-best-35mm-lenses-for-portrait-photography/";
        private static string _htmlCode;
        private static string URL;

        static void Main(string[] args)
        {
            if (args.Length == 1)
                URL = args[1];
            else
                URL = _url;
            
            using (WebClient client = new WebClient())
            {
                _htmlCode = client.DownloadString(URL);

                var imageRegex = new Regex(Reg);
                var matches = imageRegex.Matches(_htmlCode);

                if (matches.Count > 0)
                {
                    foreach (Match match in matches)
                    {
                        
                        var imageUrl = new Regex(ImageReg).Match(match.ToString())
                            .ToString().Replace("\"","");
                        string tempFileName = Path.GetTempPath() + Guid.NewGuid() + ".jpg";
                        client.DownloadFile(new Uri(imageUrl), tempFileName);
                        
                    }
                }
            }
        }
    }
}